﻿using DeOnibusTest.Domain.Entities;
using System.Collections.Generic;
using System.Linq;

namespace DeOnibusTest.Domain.Interfaces
{
    public interface IRepository
    {
        void Add<TEntity>(TEntity entity) where TEntity : BaseEntity;

        void Update<TEntity>(TEntity entity) where TEntity : BaseEntity;

        void SaveChanges();

        int SaveOrUpdate<TEntity>(TEntity entity) where TEntity : BaseEntity;

        void Delete<TEntity>(TEntity entity) where TEntity : BaseEntity;

        void Delete(IEnumerable<object> entities);

        void Delete<TEntity>(int id) where TEntity : BaseEntity;

        IQueryable<TEntity> Query<TEntity>() where TEntity : BaseEntity;

        TEntity GetById<TEntity>(int id) where TEntity : BaseEntity;

        IQueryable<TEntity> QueryAsNoTracking<TEntity>() where TEntity : BaseEntity;


    }
}
