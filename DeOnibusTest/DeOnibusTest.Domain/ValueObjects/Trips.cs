﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeOnibusTest.Domain.ValueObjects
{
    public class Trips
    {

        public string ObjectId { get; set; }
        public Company Company { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }

        public ApiDate DepartureDate { get; set; }
        public ApiDate ArrivalDate { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public Decimal Price { get; set; }
        public String BusClass { get; set; }

        public bool BetterPrice { get; set; }

    }
}
