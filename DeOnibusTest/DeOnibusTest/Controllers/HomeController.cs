﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DeOnibusTest.Domain.ValueObjects;
using DeOnibusTest.Domain.Interfaces;
using DeOnibusTest.Domain.Entities;
using System.Linq;

namespace DeOnibusTest.Controllers
{
    public class HomeController : Controller
    {

        public HomeController()
        {          
        }

        public IActionResult Index()
        {
            return View();
        }

    }
}
