﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DeOnibusTest.Domain.ValueObjects;
using DeOnibusTest.Domain.Interfaces;
using DeOnibusTest.Domain.Entities;
using System.Linq;

namespace DeOnibusTest.Controllers
{
    public class TripController : Controller
    {

        private readonly Service.Services.FavoriteTrips _favoriteTrips;

        public TripController(IRepository repository)
        {
            _favoriteTrips = new Service.Services.FavoriteTrips(repository);
        }

        public async Task<IActionResult> Index()
        {
            ViewBag.Classes = await Service.Services.ConsumeAPI.GetClasses();
            return View();
        }

        public async Task<ActionResult> GetTrips(List<string> Classes, int Departure, decimal MaxPrice)
        {
            List<Trips> trips = await Service.Services.ConsumeAPI.GetTrips(Classes, Departure, MaxPrice);
            List<string> favoriteTripsIds = await _favoriteTrips.GetFavoriteTripsIds();

            trips = trips.Where(x => !favoriteTripsIds.Contains(x.ObjectId)).ToList();

            return PartialView("_Trips", trips);
        }

        public async Task<ActionResult> GetFavoriteTrips()
        {
            List<Trips> trips = await Service.Services.ConsumeAPI.GetTrips(null, 0, 0);
            List<string> favoriteTripsIds = await _favoriteTrips.GetFavoriteTripsIds();

            trips = trips.Where(x => favoriteTripsIds.Contains(x.ObjectId)).ToList();

            return PartialView("_FavoriteTrips", trips);
        }

        public ActionResult SaveFavoriteTrips(List<string> SelectedTrips)
        {
            try
            {
                foreach (var trip in SelectedTrips)
                {
                    _favoriteTrips.SaveOrUpdate(new FavoriteTrip() { TripId = trip });
                }
                return Ok();
            }
            catch (System.Exception ex)
            {
                return Problem(detail: ex.Message, title: "Erro");
            }
        }

        public async Task<ActionResult> RemoveFavoriteTrips(string TripId)
        {
            try
            {
                var favorite = await _favoriteTrips.GetByTripId(TripId);
                _favoriteTrips.Delete(favorite);
                return Ok();
            }
            catch (System.Exception ex)
            {
                return Problem(detail: ex.Message, title: "Erro");
            } 
        }

    }
}
