﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace DeOnibusTest
{
    public static class HtmlHelpers
    {

        public static IEnumerable<SelectListItem> GetSelectList(this IHtmlHelper html, List<string> itens)
        {
            var lista = new List<SelectListItem>();
            foreach (var item in itens)
            {
                lista.Add(new SelectListItem(item, item));
            }
            return lista;
        }

    }
}
