﻿
function ExecutarAjax(url, data, type, successCallback, errorCallback) {
    $.ajax({
        url: url,
        data: data,
        type: type,
        success: successCallback,
        error: errorCallback
    });
}
