﻿
confirm = function (titulo, texto, callback) {
    Swal.fire({
        title: titulo,
        text: texto,
        icon: 'question',
        showCancelButton: true,
        confirmButtonText: '<i class="fa fa-thumbs-up"></i> Sim!',
        confirmButtonColor: "#1AB394",
        cancelButtonText: '<i class="fa fa-thumbs-down"></i> Não',
        cancelButtonColor: "#ED5565",
    }).then((result) => {
        if (callback) {
            if (result.isConfirmed) {
                callback(true);
            } else {
                callback(false);
            }
        }
    });
}

alert = function (titulo, texto, icon, callback) {
    Swal.fire({
        icon: icon,
        title: titulo,
        text: texto
    }).then((result) => {
        if (callback) {
            callback(true);
        }
    });
}