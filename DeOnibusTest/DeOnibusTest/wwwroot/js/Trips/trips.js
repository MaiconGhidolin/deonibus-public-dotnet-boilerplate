﻿
function GetTrips() {
    var dados = {
        Classes: $("#selFilterClasses").val(),
        Departure: $("#selFilterDeparture").val(),
        MaxPrice: $("#txtMaxPrice").val()
    };
    ExecutarAjax("/Trip/GetTrips", dados, "POST", function (html) {
        $('#divTrips').html(html);
    }, function (a, b, c) {
        alert("Atenção", "Houve um erro ao buscar as viagens", "warning");
    });
}

function GetFavoriteTrips() {
    ExecutarAjax("/Trip/GetFavoriteTrips", null, "POST", function (html) {
        $('#divFavoriteTrips').html(html);
    }, function (a, b, c) {
        alert("Atenção", "Houve um erro ao buscar as viagens favoritas", "warning");
    });
}

function SelectTrip(btn, tripId) {
    var trips = new Array();
    if ($("#hddSelectedTrips").val() != "") {
        trips = JSON.parse($("#hddSelectedTrips").val());
    }

    if ($(btn).text() == "Selecionado") {
        $(btn).text("Selecionar");
        $(btn).removeClass("btn-primary");
        $(btn).addClass("btn-success");

        trips.splice(trips.indexOf(tripId), 1);
    } else {
        $(btn).text("Selecionado");
        $(btn).removeClass("btn-success");
        $(btn).addClass("btn-primary");

        trips.push(tripId);
    }

    if (trips.length > 0) {
        $("#divBtnFavoriteTrips").show();
    } else {
        $("#divBtnFavoriteTrips").hide();
    }

    $("#hddSelectedTrips").val(JSON.stringify(trips));
}

function SaveFavoriteTrips() {
    var dados = {
        SelectedTrips: JSON.parse($("#hddSelectedTrips").val())
    };
    ExecutarAjax("/Trip/SaveFavoriteTrips", dados, "POST", function (ret) {
        window.location.reload();
    }, function (a, b, c) {
        alert("Atenção", "Houve um erro ao salvar as viagens favoritas", "warning");
    });
}

function RemoveFavoriteTrip(tripId) {
    var dados = {
        TripId: tripId
    };
    ExecutarAjax("/Trip/RemoveFavoriteTrips", dados, "POST", function (ret) {
        window.location.reload();
    }, function (a, b, c) {
        alert("Atenção", "Houve um erro ao remover a viagem favorita", "warning");
    });
}