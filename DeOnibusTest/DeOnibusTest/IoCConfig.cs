﻿using DeOnibusTest.Domain.Interfaces;
using DeOnibusTest.Infra;
using DeOnibusTest.Infra.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace DeOnibusTest
{
    public static class IoCConfig
    {
        public static void RegisterDependencies(IServiceCollection services)
        {
            services.AddScoped<IRepository, RepositorioGenerico>();
            services.AddScoped<EFContext>();
            services.AddScoped<DbContext>(x => x.GetRequiredService<EFContext>());
        }

    }
}
