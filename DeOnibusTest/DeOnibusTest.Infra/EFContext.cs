﻿using System.Reflection;
using Microsoft.EntityFrameworkCore;

namespace DeOnibusTest.Infra
{
    public class EFContext : DbContext
    {

        public EFContext(DbContextOptions<EFContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(EFContext).GetTypeInfo().Assembly);

            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string stringDeConexao = "Server=(localdb)\\mssqllocaldb;Database=DeOnibusTeste;Trusted_Connection=True;MultipleActiveResultSets=true";

            optionsBuilder
                .UseSqlServer(stringDeConexao, b => b.MigrationsAssembly("DeOnibusTest"));

            optionsBuilder.UseLazyLoadingProxies();
            base.OnConfiguring(optionsBuilder);
        }

    }
}
