﻿using DeOnibusTest.Domain.Extensions;
using DeOnibusTest.Domain.ValueObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace DeOnibusTest.Service.Services
{
    public static class ConsumeAPI
    {

        public static async Task<List<Trips>> GetTrips(List<string> Classes, int Departure, decimal MaxPrice)
        {
            List<Trips> trips = new List<Trips>();
            using (var httpClient = new HttpClient())
            {
                using var response = await httpClient.GetAsync("https://4jehkg0izj.execute-api.us-east-1.amazonaws.com/stage-v0/route");
                string apiResponse = await response.Content.ReadAsStringAsync();
                ApiResult apiResult = JsonConvert.DeserializeObject<ApiResult>(apiResponse);

                trips = apiResult.Results.OrderBy(x => x.DepartureDate.Iso).ToList();

                if (Classes.IsNotNullOrEmpty())
                    trips = trips.Where(x => Classes.Contains(x.BusClass)).ToList();

                if (Departure > 0)
                {
                    TimeSpan startDay = new TimeSpan(0, 0, 0);
                    TimeSpan six = new TimeSpan(6, 0, 0);
                    TimeSpan noon = new TimeSpan(12, 0, 0);
                    TimeSpan eighteen = new TimeSpan(18, 0, 0);
                    TimeSpan endDay = new TimeSpan(23, 59, 59);

                    if (Departure == 1)
                        trips = trips.Where(x => x.DepartureDate.Iso.TimeOfDay >= startDay && x.DepartureDate.Iso.TimeOfDay < six).ToList();
                    else if (Departure == 2)
                        trips = trips.Where(x => x.DepartureDate.Iso.TimeOfDay >= six && x.DepartureDate.Iso.TimeOfDay < noon).ToList();
                    else if (Departure == 3)
                        trips = trips.Where(x => x.DepartureDate.Iso.TimeOfDay >= noon && x.DepartureDate.Iso.TimeOfDay < eighteen).ToList();
                    else if (Departure == 4)
                        trips = trips.Where(x => x.DepartureDate.Iso.TimeOfDay >= eighteen && x.DepartureDate.Iso.TimeOfDay <= endDay).ToList();
                }

                if (MaxPrice > 0)
                    trips = trips.Where(x => decimal.Round(x.Price, 2, MidpointRounding.AwayFromZero) <= MaxPrice).ToList();

                if (trips.IsNotNullOrEmpty())
                {
                    var minPrice = trips.Min(x => x.Price);
                    trips.Where(x => x.Price == minPrice).ToList().ForEach(x => x.BetterPrice = true);
                }

            }
            return trips;
        }

        public static async Task<List<string>> GetClasses()
        {
            List<string> classes = new List<string>();
            using (var httpClient = new HttpClient())
            {
                using var response = await httpClient.GetAsync("https://4jehkg0izj.execute-api.us-east-1.amazonaws.com/stage-v0/route");
                string apiResponse = await response.Content.ReadAsStringAsync();
                ApiResult apiResult = JsonConvert.DeserializeObject<ApiResult>(apiResponse);

                classes = apiResult.Results.Select(x => x.BusClass).Distinct().OrderBy(x => x).ToList();
            }
            return classes;
        }

    }
}
