﻿using DeOnibusTest.Domain.Interfaces;
using DeOnibusTest.Domain.ValueObjects;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeOnibusTest.Service.Services
{
    public class FavoriteTrips
    {

        private readonly IRepository _repository;

        public FavoriteTrips(IRepository repository)
        {
            _repository = repository;
        }

        public async Task<List<string>> GetFavoriteTripsIds()
        {
            var result = await _repository
                .Query<Domain.Entities.FavoriteTrip>()
                .Select(x => x.TripId)
                .ToListAsync();

            return result;
        }

        public async Task<Domain.Entities.FavoriteTrip> GetByTripId(string tripId)
        {
            var result = await _repository
                .Query<Domain.Entities.FavoriteTrip>()
                .Where(x => x.TripId == tripId)
                .FirstOrDefaultAsync();

            return result;
        }

        public void SaveOrUpdate(Domain.Entities.FavoriteTrip favoriteTrip)
        {
            _repository.SaveOrUpdate(favoriteTrip);
        }

        public void Delete(int id)
        {
            _repository.Delete<Domain.Entities.FavoriteTrip>(id);
        }

        public void Delete(Domain.Entities.FavoriteTrip favoriteTrip)
        {
            _repository.Delete(favoriteTrip);
        }


    }
}
